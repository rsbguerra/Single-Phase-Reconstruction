# Single-Phase-Reconstruction
Software aiming to reconstruct color holograms using only one of the phase's information


## Project organization

```
│
├── bin/
│   └── codecs/
├── data/
│   ├── config/
│   ├── input/
│   └── output/
├── res/
│   ├── ctc/
│   └── nrsh/
├── src/
│   ├── holo_config/
│   ├── reconstruction/
│   └── utils/
└── utils/
```

- `bin/` ─ Contains codec binaries and scripts used to use them;
- `data/` ─ Input and output folders;
- `res/` ─ External projects location;
- `src/` ─ Source code.