#Format specifics
representation	: "complex"
datatype	: "float"
dimension       : [2048,16384]

#HOLOGRAM SPLITTING#
tile_size       : [2048,16384]
transform_block_size        : [512,512]
# Format 4D: fx, fy, x, y
code_block_size: [512,512,1,1]
quantization_block_size: [4,4,1,1]

#RECONSTRUCTION PARAMETERS#
wlen       : [5.320000e-07]
pixel_pitch       : ([3.450000e-06])

