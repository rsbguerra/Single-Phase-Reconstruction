#Format specifics
representation	: "complex"
datatype	: "float"
dimension       : [2048,20480]

#HOLOGRAM SPLITTING#
tile_size       : [2048,20480]
transform_block_size        : [2048,2048]
# Format 4D: fx, fy, x, y
code_block_size: [2048,2048,1,1]
quantization_block_size: [8,8,1,1]

#RECONSTRUCTION PARAMETERS#
wlen       : [6.328000e-07]
pixel_pitch       : ([3.450000e-06])

