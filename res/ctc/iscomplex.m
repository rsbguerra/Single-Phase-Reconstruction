function res = iscomplex(x)
    res = ~isreal(x);
end
