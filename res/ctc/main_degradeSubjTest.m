%% Objective and subjective testing pipeline for JPEG 2000 (Kakadu 8.0.5) + H.265/HEVC (HM 16.20)
%
%% Comments on inner workings
% - Uses an exhaustive QP binary search for rate matching per color channel in case of H.265/HEVC.
% - JPEG 2000 is used at the best approximative rates that H.265/HEVC achieved per color channel.
% - ObjTest and subjTest will by default use different viewpoints, which may be changed via holoread_PL and in this
%       script, respectively. Separate sets of reconstructions will be created for either case.
%
%% Comments on Configurations
% - An arbitrary list of bitrates may be specified.
% - An arbitrary list of distortions may be specified, and implemented in Pipeline_Compress.m +
%       changing in-/outfile prefixes in ObjEvaluateCompressedHolograms.m, ST_RenderCompressedHolograms.
% - Any selection of Compression, objectiveTest, subjectiveTest (to be processed) may be done via "doCompression",
%       "doObjTest", "doSubjTest".
% - Compressed data may be forcibly removed via "doDiscardPreviouslyCompressedData".
% - Diffraction Limited reconstructions may be activated with the flag "doLowResSubjTest". Note, compatible with
%           doSubjectiveTestVideo == true.
%       Note: Diffraction limited and regular reconstructions may be kept simultaneously. Files will get "_LR" suffix in the former case.
% - Subjective Test video sequences may be activated with the flag "doSubjectiveTestVideo".
%
%% Comments on viewpoints/reconstructions wrt. objTest, subjTest
% - Subjective test videos are generated by following a list of viewpoints. Please specify, in Hsubj: rec_dists, ap_sizes,
%       h_pos, v_pos accordingly. Per quantity: either as a list/cell array of the length of viewpoints or scalar.
%       See also input argument specification of nrsh_video.m.
% - By default clipping of all reconstructions is done at the same absolute thresholds as for the ground_truth.
%   For the subjective test videos, the default is clipping according to the first frame.
% - By default for "doLowResSubjTest = false" test videos will be bi-linearly resized to 2048x2048.
% - "doLowResSubjTest = true" will calculate appropiate bounds for non-Fourier holograms (i.e. not wut_disp_on_axis, interfere4).
%       For Fourier holograms a bi-linear resize to the size specified in the nrsh-config files (recons_img_size) or
%       min(hol_rendered)*[1,1] will be performed, using the assumption bandwidth == aperture size.
%
%% Comments on OUTPUT
% - A logfile will be written for the main_degrade script in pwd().
% - All files written by NRSH, reside in pwd()/figures.
% - All NRSH files are prefixed, e.g. ObjTest_holo_JPEG2000_10bpp_astronaut_000_0_0_[2588x2588]_-0.172.png.
% - All subjective test videos are prefixed as well and are written to pwd()/figures, incl. a separate log file.
%       Temporary frames will be deleted. Videos will be names as, e.g.: SubjTest_GT__astronaut_000_nFrames7_at20FPS_LR.mp4
%       Videos will be written with H.264/AVC using ffmpeg -c:v libx264 -qp 0
%
% - All compressed files are written to "Folders.Forkfolder=['plenofolder/' plane 'Plane/' distStr '/']]" to be specified below.
% - The configurations (viewpoints etc.) of the objTest and subjTest are written to plenofolder/{CfgObjTest.m, CfgSubjTest.m}.
% - Quanitzation information is kept in plenofolder/QuantInfo_{holo,obj}Plane.mat.
% - All objective ratings are written to plenofolder/Metrics.mat.
%       It contains:
%           "RA"@numeric... bitrate list
%           "distL"...      distortion list@cell)
%           "M"@struct...   rating scores structured as follows:
%           M.(plane).(distStr).bitrateMap@table...     map of bitrates achieved, in the same order as sort("RA")
%           M.(plane).(distStr).qpnMap@table...         map of searched QPNs to achieved bitrates (for distStr=="hm" only)
%           M.(plane).(distStr).{ref,deg}@struct-array... objective scores wrt. to ground truth non-quantized ("ref") and 16bit
%                                                                   quantized ("deg")
%                                                       The array has length numel("RA") and follows sort("RA").
%           M.(plane).(distStr).{ref,deg}.{ssnr_hol,ssim_hol}@numerics...   metrics evaluated in the hologram plane (per color channel)
%           M.(plane).(distStr).{ref,deg}.view_results@table...             metrics evaluated on the various viewpoints (per color channel)+
%                                                                               averages over viewpoints
%
%% Further comments
% - This pipeline is based on NRSHv5.
% - No separate propagations for complex-valued wavefields to the object/hologram plane are used anymore.
%
% - ATTENTION: Please install Kakadu first.
%   https://kakadusoftware.com/downloads/
% - ATTENTION: Please place ffmpeg.exe first to: Folders.nrshfolder/../ffmpeg/ffmpeg.exe
%   https://ffmpeg.org/download.html
% - ATTENTION: Please install JPEG-XL binaries, if needed.
%
%
%% How to use:
%   0) Specify what to do (Compress, SubjTest, ObjTest) and how to do it using the flags provided in 0.1).
%   1.1) Specify bitrates.
%   1.2) Specify distortions to be studied.
%   1.3) Specify directories.
%   1.4) Specify filenames/holograms to be processed.
%   ATTENTION: Ensure that holoread_PL.m contains approiate information for objTest viewpoints + is read to load your
%           hologram.
%
%
% Rewrite: T. Birnbaum
% Based on version 1.4 by Raees K. M. and T. Birnbaum
% ETRO-VUB, imec
% Version 2.4, modular, low RAM complexity, minimal HDD footprint
% 07.07.2021

clear all;clc;

% Layman's check for octave compatible mode
isOctave = false;

try
    a = datetime; clear a;
    contains('abc', 'a');
    isOctave = false;
catch me
    pkg load image;
    confirm_recursive_rmdir(0);
    isOctave = true;
end

% Enable logging
if (isOctave)
    diary(['Log_main_degrade_run_' strrep(strrep(char(date), ':', '-'), ' ', '_') '.txt'])
else
    diary(['Log_main_degrade_run_' strrep(strrep(char(datetime), ':', '-'), ' ', '_') '.txt'])
end

diary on

try

    %% 0) Initialization
    %% 0.1) Set parameters
    doCompression = false; % Includes: Encoding and Decoding path
    doDecompressionOnly = true; % Enable Encoding + Decoding or only Decoding
    doDiscardPreviouslyCompressedData = false; % ATTENTION: Enable with caution. Will discard all previously gathered data in folder structure.
    % ATTENTION: When replacing data of "original" DHs in place (without renaming) always enable.
    % ATTENTION: Enable (at least for obj-plane compression) when modifying reference distances for object plane compression.
    doObjTest = false;
    doSubjTest = true; % Ensure that Hsubj is correctly set per dataset in holoread_PL.m
    doLowResSubjTest = true; % Enable diffraction limited resizing for the subjective test; Attention: Need to specify Hsubj.targetRes!
    doSubjTestRatingOnly = false; % True: Only rate subj. test; False: Reconstruct + rate (should be default)
    doSubjectiveTestVideo = false; % Switches between nrsh and nrshVid in subjective test
    if (doDiscardPreviouslyCompressedData && ~doCompression), error('main_degrade:insufficent_data', 'There will be no data to process. Either keep former cycles or compress new.'), end
    if ((doObjTest || doSubjTest) && isOctave), warning('CTC:ssim_octave', 'The original ssim implementation is not comparable with the proprietary Matlab implementation.'), end

    %% 0.2) Quantization description
    % Don't touch - start
    L = 2 ^ 16; % because of 16bit intermediate representation of anchor codec input
    quantmethod = 'MRQ';
    optmethod = 'Hybrid';
    % Don't touch - end

    %% 1) Configure test
    %% 1.1a) Specify bitrates to be tested
    % Don't modify - start
    %bitrates for the objective QA, set based on table 6 in the CTC document:
    RA1 = [0.1 0.25 0.5 1 2 4]; %This is the bitrate set for the monochrome holograms
    RA3 = [0.3 0.75 1.5 3 6 12]; %This is the bitrate set for the color holograms
    % Don't modify - end

    %% 1.1b) Don't Modify - Start
    %bitrates for the subjective QA, set based on table 7 in the CTC document:
    RAsubj = {{'DeepCornellBox_16k', [0.1 0.5 2]}, ... % Subjective test 28.9.2021
               {'DeepDices2k', [0.75 3 12]}, ...
                   {'Lowiczanka_doll', [0.75 3 6]}, ...
                   {'Astronaut', [0.1 0.25 0.5]}, ...
                   {'DeepChess', [0.1 0.5 2]}, ...
                   {'Biplane16k-1', [0.3 1.5 6]}, ...
                   {'Dices16k', [0.3 1.5 6]}, ...
                   {'Piano16k', [0.3 1.5 6]} ...
               };
    % RAsubj = {{'DeepCornellBox_16k', [0.1 0.25 1]}, ...
    %           {'DeepDices2k', [0.75 1.5 3]}, ...
    %           {'Lowiczanka_doll', [0.3 0.75 3]}, ...
    %           {'Astronaut', [0.1 0.25 0.5]}, ...
    %           {'DeepChess', [0.1 0.5 2]}, ...
    %           {'Biplane16k-1', [0.3 3 6]}, ...
    %           {'Dices16k', [0.75 1.5 3]}, ...
    %           {'Piano16k', [0.75 1.5 3]}...
    %           };
    % RA1 = [0.1 0.25 1];    % 1-DeepCornellBox16K
    % RA3 = [0.75 1.5 3];    % 2-DeepDices2k
    % RA3 = [0.3 0.75 3];    % 3-Lowiczanka Doll
    % RA1 = [0.1 0.25 0.5];  % 4-Astronaut
    % RA1 = [0.1 0.5 2];     % 5-DeepChess
    % RA3 = [0.3 3 6];       % 6-Biplane16K - Interfere
    % RA3 = [0.75 1.5 3];    % 7-Dices16K
    % RA3 = [0.75 1.5 3];    % 8-Piano16K
    % Dont't modify - End

    %% 1.2) Specify codecs to be tested (implemented in Pipeline_Compress)
    % Anchors: distL = {'hm', 'j2k', 'vtm'};
    distLanchors = {'hm', 'j2k'}; % will be processed in obj + holo plane
    % Possible codecs: distL = {'vtm', 'xxx'}
    %           xxx = any proposed codec implemented according to: README.md
    distLextra = {'jpeg_pleno_holo_etro'}; % %{'proponentTemplate'}; % will be processed in holo plane only
    %distL = [distLanchors, distLextra];
    distL = [distLanchors, distLextra];

    %% 1.3) Set paths
    %Add nrsh base path
    addpath(genpath('..\nrsh'))
    %Main inputs
    %Specify location of folder containing input holograms
    Folders.holofolder = 'D:\Rating_OrgData\';
    %Specify location of NRSH config file location
    Folders.nrshfolder = '..\nrsh\config_files';
    %Specify location of HM executable
    Folders.codecfolder = '..\codecs';
    %Specify location for already encoded inputs, in case of doDecompression only
    Folders.encfolder = 'd:/Rating_EncData';

    if (isOctave)
        tmpfold = 'tmpOctave';
    else
        tmpfold = 'tmpMatlab';
    end

    %Specify location of result
    plenofolder = fullfile('D:\Rating_OutData\'); %fullfile(['E:\' tmpfold '\PlenoFinalCompsSubjectiveTest\Results']);
    % NRSH output folder for *distorted* reconstructions
    Folders.nrshOutFolderDist = fullfile(plenofolder, 'NrshOut'); %fullfile(['d:/' tmpfold '/NrshOutTmp']);
    %Specify outer foldername of result, used as plenofolder/Outerfoldername
    Outerfoldername = 'JPEG_Pleno_subj';
    count = 1;

    %% 1.4) Uncomment filename(s) to be tested
    holofile(count).strng = 'Astronaut_Hol_v2.mat'; fragmentplenofolder2(count).strng = 'Astronaut';
    count = count + 1;
    %
    % holofile(count).strng='horse_Hol_v1.mat';fragmentplenofolder2(count).strng= 'Horse';
    % count = count+1;
    %
    % holofile(count).strng='cgh_ball_slit_AS_ccd_plane.mat';fragmentplenofolder2(count).strng= 'Ball';
    % count = count+1;
    %
    % holofile(count).strng='CornellBox3_16K.mat';fragmentplenofolder2(count).strng= 'CornellBox3_16k';
    % count = count+1;
    %
    holofile(count).strng = 'DeepCornellBox_16K.mat'; fragmentplenofolder2(count).strng = 'DeepCornellBox_16k';
    count = count + 1;
    %
    % holofile(count).strng='cgh_chess2_slit_AS_ccd_plane.mat';fragmentplenofolder2(count).strng= 'Chess';
    % count = count+1;
    %
    holofile(count).strng = 'DeepChess.mat'; fragmentplenofolder2(count).strng = 'DeepChess';
    count = count + 1;

    % holofile(count).strng='opt_Sphere.mat';fragmentplenofolder2(count).strng= 'Sphere';
    % count = count+1;
    %
    % holofile(count).strng='opt_Squirrel.mat';fragmentplenofolder2(count).strng= 'Squirrel';
    % count = count+1;
    %
    holofile(count).strng = 'CGH_Biplane16k_rgb.mat'; fragmentplenofolder2(count).strng = 'Biplane16k-1'; % Interfere
    count = count + 1;
    %
    % holofile(count).strng='biplane16k.mat';fragmentplenofolder2(count).strng= 'Biplane16k-2'; % Bcom
    % count = count+1;
    %
    holofile(count).strng = 'DeepDices2K.mat'; fragmentplenofolder2(count).strng = 'DeepDices2k';
    count = count + 1;
    %
    % holofile(count).strng='DeepDices8K4K.mat';fragmentplenofolder2(count).strng= 'DeepDices8k4k';
    % count = count+1;
    %
    % holofile(count).strng='DeepDices16K.mat';fragmentplenofolder2(count).strng= 'DeepDices16k';
    % count = count+1;
    %
    holofile(count).strng = 'Dices16K.mat'; fragmentplenofolder2(count).strng = 'Dices16k';
    count = count + 1;
    %
    % holofile(count).strng='SpecularCar16K.mat';fragmentplenofolder2(count).strng= 'SpecularCar16k';
    % count = count+1;
    %
    holofile(count).strng = 'Piano16K.mat'; fragmentplenofolder2(count).strng = 'Piano16k';
    count = count + 1;
    %
    % holofile(count).strng='Ring16K.mat';fragmentplenofolder2(count).strng= 'Ring16k';
    % count = count+1;
    %

    holofile(count).strng = 'opt_Warsaw_Lowiczanka_Doll.mat'; fragmentplenofolder2(count).strng = 'Lowiczanka_doll';
    count = count + 1;
    %
    % holofile(count).strng='opt_Warsaw_Mermaid.mat';fragmentplenofolder2(count).strng= 'Warsaw_Mermaid';
    % count = count+1;
    %
    % holofile(count).strng='breakdancers8k4k_022.mat';fragmentplenofolder2(count).strng= 'Breakdancers8k4k_022';
    % count = count+1;
    %
    % holofile(count).strng='ballet8k4k_022.mat';fragmentplenofolder2(count).strng= 'Ballet8k4k_022';
    % count = count+1;

    % For pipeline testing only

    % holofile(count).strng='Dices4K.mat';fragmentplenofolder2(count).strng= 'Dices4k';
    % count = count+1;
    %
    % holofile(count).strng='Dices8K.mat';fragmentplenofolder2(count).strng= 'Dices8k';
    % count = count+1;
    %
    % holofile(count).strng='breakdancers8k4k_022.mat';fragmentplenofolder2(count).strng= 'Breakdancers8k4k';
    % count = count+1;

    %% Start the work
    distL = distL(:).';

    for j = 1:size(holofile, 2)
        %% 2) Setup up ground truth of chosen hologram
        %% 2.1) Set folder paths
        Folders.holoname = fragmentplenofolder2(j).strng;
        Folders.holofile = holofile(j).strng;
        Folders.plenofolder = fullfile(plenofolder, Outerfoldername, fragmentplenofolder2(j).strng);
        makefolder(Folders.plenofolder)
        Folders.esfolder = fullfile(Folders.plenofolder, 'plots');
        disp(['Processing ' Folders.holofile '...'])
        tLocHologram = tic();

        %% 2.2) Discard unquantized GT & TestCFG data & move metrics.mat
        if (doDiscardPreviouslyCompressedData)
            fnameL = {fullfile(Folders.plenofolder, 'input.mat')};
            if (doObjTest), fnameL = [fnameL, {fullfile(Folders.plenofolder, 'CfgObjTest.mat')}]; end
            if (doSubjTest), fnameL = [fnameL, {fullfile(Folders.plenofolder, 'CfgSubjTest.mat')}]; end

            warning('off', 'MATLAB:DELETE:FileNotFound')

            for fnameC = fnameL

                try
                    fname = fnameC{1};
                    delete(fname)
                    disp([fname ' successfully deleted.'])
                catch me
                end

            end

            warning('on', 'MATLAB:DELETE:FileNotFound')

            try
                fname = fullfile(Folders.plenofolder, 'Metrics.mat');
                movefile(fname, strrep(fname, 'Metrics.mat', 'Metrics.old.mat'))
                disp([fname ' successfully moved to ' strrep(fname, 'Metrics.mat', 'Metrics.old.mat') '.'])
            catch me
            end

            clear fnameL fnameC fname;
        end

        %% 2.3) Read file from database
        [X, H] = holoread_PL(Folders);
        si = size(X);
        si(3) = size(X, 3);
        save(fullfile(Folders.plenofolder, 'CfgObjTest.mat'), 'H');
        clear M cminObjT cmaxObjT cminSubjT cmaxSubjT;

        %% 2.4) Assign bitrate depending on number of color channels + test configuration
        if (si(3) == 1), RA = RA1; else, RA = RA3; end

        if (doSubjTest)
            % Lookup subjective test bitrates
            idx = cellfun(@(x) strcmpi(x{1}, fragmentplenofolder2(j).strng), RAsubj);

            if (isempty(idx) || all(idx == 0))
                error('main_degrade:invalid_subj_dataset', ['The DH ''' fragmentplenofolder2(j).strng ''' is not part of the subjective test. Please either comment dataset out or set doSubjTest = false.'])
            end

            RAsubjLoc = RAsubj{idx}{2};

            if (doObjTest)
                RA = unique([RA, RAsubjLoc]); % Combine subj. and obj. test bitrates
            else
                RA = RAsubjLoc; % Replace obj. test bitrates with subj. test bitrates
            end

        end

        %% Do the work
        % Pipeline fork 1 - Hologram plane compression -------------------------------------------
        % Pipeline fork 2 - Object plane compression ---------------------------------------------
        for planeC = {'holo', 'obj'}
            % Obj plane will be automatically skipped for non-anchor codecs, by modification of distLrun
            plane = planeC{1};
            Folders.forkfolder = fullfile(Folders.plenofolder, [plane 'Plane']);

            if (doDiscardPreviouslyCompressedData)
                %% Clean up temporary files
                warning('off', 'MATLAB:DELETE:FileNotFound')

                for distC = distL
                    folder = fullfile(Folders.plenofolder, [plane 'Plane'], cell2mat(distC));

                    try
                        rmdir(folder, 's');
                        disp([folder ' successfully deleted.'])
                    catch me
                    end

                end

                folder = fullfile(Folders.plenofolder, [plane 'Plane'], 'temp');

                try
                    rmdir(folder, 's');
                catch me
                end

                disp([folder ' successfully deleted.'])

                try
                    delete(fullfile(Folders.plenofolder, [plane 'Plane'], 'quantref.mat'))
                catch me
                end

                warning('on', 'MATLAB:DELETE:FileNotFound')

                for ii = 1:2, disp(" "), end
            end

            try
                mkdir(Folders.forkfolder)
            catch me
            end

            disp(['Processing ' Folders.holofile ' in ' plane ' plane...'])
            tLocPlane = tic();

            %% 3) Compression
            %% 3.0) Load QPN/Bitrate maps from last compression cycle, if possible
            if (exist(fullfile(Folders.plenofolder, 'Metrics.mat'), 'file'))
                datTmp = load(fullfile(Folders.plenofolder, 'Metrics.mat'), 'M', 'RA', 'H');

                M = datTmp.M;

                if (~isempty(setdiff(RA, datTmp.RA)) && ~isempty(setdiff(fieldnames(M.(plane)), distL)) && ~doCompression) % check, if all requested bitrates/distortions have been previously compressed
                    error('main_degrade:invalid_mode', 'Compression required, as new bitrates are requested.')
                end

            end

            if (~exist('M', 'var'))
                M = struct();
            end

            if (doCompression)
                %% 3.1) Propagate to object plane evtl.
                if (strcmpi(plane, 'holo'))
                    Xmod = X;
                else
                    zeropad = false; % Faciliate: nopad approach for anchors, see wg1m89073
                    info = getSettings('dataset', H.dataset, 'zero_pad', zeropad, 'cfg_file', fullfile(Folders.nrshfolder, H.cfg_file), 'usagemode', 'complex', 'direction', 'forward');
                    Xmod = nrsh(X, H.obj_dist, info);
                end

                %% 3.2) Quantize data + write quantized data to HDD
                if (~isempty(intersect(distL, distLanchors)) && (~exist(fullfile(Folders.plenofolder, ['QuantInfo_' plane 'Plane.mat']), 'file') || ~exist(fullfile(Folders.forkfolder, 'quantref.mat'), 'file')))
                    [Q, Xmod, Xpoi] = Quantize_PL(Xmod, L, quantmethod, optmethod);
                    save(fullfile(Folders.plenofolder, ['QuantInfo_' plane 'Plane.mat']), 'Xpoi', 'quantmethod', 'optmethod', 'L');
                    Quarefwrite_PL(Q, Xmod, Xpoi, L, quantmethod, Folders); % Write quanitzed data, as input for compress functions
                    clear Q Xmod Xpoi;
                end

                %% 3.3) Compress
                if (~isfield(M, plane))
                    M.(plane) = struct();
                end

                %% Skip object plane for non-anchors
                distLrun = distL;

                if (strcmpi(plane, 'obj'))
                    distLrun = intersect(distL, distLanchors);
                end

                M.(plane) = Pipeline_Compress(Folders, RA, distLrun, M.(plane), doDecompressionOnly);
                % M.(plane) = Pipeline_Compress(Folders, RA, distL, M); % Only
                % for manual debugging purposes.
                disp(['  Compression done in ' num2str(toc(tLocPlane)), ' s.'])

                %% 3.4) Save results (QPN/Bitrate map)
                save(fullfile(Folders.plenofolder, 'Metrics.mat'), '-v6', 'M', 'RA', 'H', 'distL', 'distLanchors');
            else
                distLtmp = fieldnames(M.(plane));

                for distC = distL(:).'
                    isAnchor = ~isempty(intersect(distLanchors, distC));
                    if (~isAnchor && strcmpi(plane, 'obj')), continue, end

                    if (sum(strcmpi(distLtmp, distC)) == 0)
                        error('main_degrade:compression_skip', [Folders.holofile ' was not compressed with ' distC{1} ' in ' plane ' plane. Need to enable compress on next run.'])
                    end

                end

            end

            %% 4) Perform objective test reconstruction, if enabled
            if (doObjTest)
                disp(['Processing ' Folders.holofile ' in ' plane ' plane - Objective Test - ...'])
                tLocObj = tic();

                Hobj = H;
                Hobj.doDynamic = false;

                %% Specify objective test viewpoints.
                % Modify "Hobj" here, if same viewpoints should be tested/reconstructed for all datasets.
                %             Hobj.rec_dists = Hobj.rec_dists;
                %             % Hobj.rec_dists = Hobj.rec_dists(round(end/2));
                %             Hobj.h_pos = [0, 1];
                %             Hobj.v_pos = [0, 1];
                %             Hobj.ap_sizes = {[size(X(:,:,1))/2]};
                %% End "Hobj" mofiy

                %% 4.1) Reconstruct ground truth for cmin, cmax
                info = getSettings('dataset', Hobj.dataset, 'cfg_file', fullfile(Folders.nrshfolder, H.cfg_file), 'name_prefix', ['ObjTest_' plane '_GT_']); % GT located in 'figures'
                [~, cminObjT.(plane), cmaxObjT.(plane)] = nrsh(X, Hobj.rec_dists, info, usagemodeFun(Hobj.doDynamic), Hobj.ap_sizes, Hobj.h_pos, Hobj.v_pos);
                %              load(fullfile(Folders.plenofolder, 'CfgSubjTest.mat'), 'cminSubjT', 'cmaxSubjT');
                %              cminObjT = cminSubjT;
                %              cmaxObjT = cmaxSubjT;
                save(fullfile(Folders.plenofolder, 'CfgObjTest.mat'), '-v6', 'cminObjT', 'cmaxObjT', 'Hobj', 'H');

                %% 4.2) Get quantized ground truth into memory, if anchor is present
                if (~isempty(intersect(distL, distLanchors)))
                    fname = fullfile(Folders.forkfolder, 'quantref.mat');
                    %% 4.2.a) Load quantized ground truth
                    if (exist(fname, 'file'))
                        Xqref = load(fname, 'Xqref');
                        Xqref = Xqref.Xqref;
                    else
                        %% 4.2.b) Redo quantization

                        %% 4.2.b.1) Propagate to object plane evtl.
                        if (strcmpi(plane, 'holo'))
                            Xmod = X;
                        else
                            zeropad = false; % Faciliate: nopad approach for anchors, see wg1m89073
                            info = getSettings('dataset', H.dataset, 'zero_pad', zeropad, 'cfg_file', fullfile(Folders.nrshfolder, H.cfg_file), 'usagemode', 'complex', 'direction', 'forward');
                            Xmod = nrsh(X, H.obj_dist, info);
                        end

                        fname = fullfile(Folders.plenofolder, ['QuantInfo_' plane 'Plane.mat']);

                        if (exist(fname, 'file'))
                            %% 4.2.b.1a) Redo quantization exactly using prior optimal clipping points
                            disp(['Objective testing: loaded quantization information from ' fname ' to redo quantization exactly.'])
                            load(fname, 'Xpoi', 'quantmethod', 'L');
                            [~, Xqref] = Quantize_PL(Xmod, L, quantmethod, Xpoi);
                        else
                            %% 4.2.b.1b) Redo quantization by recalculating optimal clipping points before
                            disp(['Objective testing: could not load quantization information from ' fname '. Redo quantization optimization.'])
                            [~, Xqref, Xpoi] = Quantize_PL(Xmod, L, quantmethod, optmethod);
                            save(fullfile(Folders.plenofolder, ['QuantInfo_' plane 'Plane.mat']), 'Xpoi', 'quantmethod', 'optmethod', 'L');
                        end

                        clear Xmod;
                    end

                    %% 4.3) Reconstruct quantized ground truth + BP quantized GT
                    info = getSettings('dataset', H.dataset, 'cfg_file', fullfile(Folders.nrshfolder, H.cfg_file), 'name_prefix', ['ObjTest_' plane '_GT16bit_']); % GT located in 'figures'

                    if (strcmpi(plane, 'obj'))
                        zeropad = false; % Faciliate: nopad approach for anchors, see wg1m89073
                        infoComplex = getSettings('dataset', H.dataset, 'zero_pad', zeropad, 'cfg_file', fullfile(Folders.nrshfolder, H.cfg_file), 'usagemode', 'complex', 'direction', 'inverse');

                        Xqref = nrsh(Xqref, H.obj_dist, infoComplex); % Propagate quantized GT from obj to holo plane
                        nrsh(Xqref, Hobj.rec_dists, info, usagemodeFun(Hobj.doDynamic), Hobj.ap_sizes, Hobj.h_pos, Hobj.v_pos, cminObjT.(plane), cmaxObjT.(plane));
                    else
                        nrsh(Xqref, Hobj.rec_dists, info, usagemodeFun(Hobj.doDynamic), Hobj.ap_sizes, Hobj.h_pos, Hobj.v_pos, cminObjT.(plane), cmaxObjT.(plane));
                    end

                else
                    Xqref = [];
                end

                %% 4.4) Rate
                M.(plane) = ObjEvaluateCompressedHolograms(Folders, M.(plane), Hobj, X, Xqref, si, cminObjT.(plane), cmaxObjT.(plane), plane, distL, distLanchors);

                %% 4.5) Save results (QPN/Bitrate map + Rating Scores)
                save(fullfile(Folders.plenofolder, ['Metrics.mat']), '-v6', 'M', 'RA', 'distL', 'distLanchors');

                disp(['Processing ' Folders.holofile ' in ' plane ' plane - Objective Test - done in ' num2str(toc(tLocObj)), ' s.'])
            end

            %% 5) Perform subjective test, if enabled
            if (doSubjTest)
                disp(['Processing ' Folders.holofile ' in ' plane ' plane - Subjective Test - ...'])
                tLocSubj = tic();

                Hsubj = H.Hsubj;
                Hsubj.doLowResolution = doLowResSubjTest;
                Hsubj.doDynamic = doSubjectiveTestVideo;
                Hsubj.doIndividual = true; % By default only explicitly specified viewpoints should be reconstructed

                %% Modify "Hsubj" here, if same viewpoints/options should be set for all datasets. Otherwise, modify Hsubj in holoread_PL.m.
                Hsubj.fps = 10;
                %             nframes = 5;
                %             Hsubj.rec_dists = linspace(Hsubj.rec_dists(1), Hsubj.rec_dists(2), nframes);
                %             Hsubj.rec_dists = Hsubj.rec_dists(round(end/2));

                %             [AperX,AperY,RecZ] = ScanpathGen(Hsubj.rec_dists,fragmentplenofolder2(1).strng,Hsubj.doDynamic);
                %             Hsubj.rec_dists = RecZ;
                %             Hsubj.h_pos = AperX;
                %             Hsubj.v_pos = AperY;

                % Either leave targetRes empty or ap_sizes empty. If both are specified, targetRes will become the objective for low_resolution reconstruction.
                Hsubj.targetRes = 2028 * [1, 1]; % Target resolution for the subjective test
                Hsubj.ap_sizes = [];
                %             %set the aperture size for holograms which does not require LR
                %             %mode
                %                 Hsubj.ap_sizes = repmat({2028*[1,1]},1,numel(AperX));
                %% End "Hsubj" mofiy

                bitrateL = RAsubj{cellfun(@(x) strcmp(x{1}, Folders.holoname), RAsubj)};
                Hsubj.bitrateL = bitrateL{2};
                clear bitrateL;

                %% 5.0) Load QPN/Bitrate maps from last subjTest cycle, if possible
                if (exist(fullfile(Folders.plenofolder, 'MetricsSubj.mat'), 'file'))
                    datTmp = load(fullfile(Folders.plenofolder, 'MetricsSubj.mat'), 'Msubj');

                    Msubj = datTmp.Msubj;

                    if (doSubjTestRatingOnly)
                        datTmp = load(fullfile(Folders.plenofolder, 'MetricsSubj.mat'), 'cminSubjT', 'cmaxSubjT');
                        cminSubjT = datTmp.cminSubjT; cmaxSubjT = datTmp.cmaxSubjT;
                    end

                end

                if (~exist('Msubj', 'var'))
                    Msubj = struct();

                    if (doSubjTestRatingOnly)
                        cminSubjT = [];
                        cmaxSubjT = [];
                    end

                end

                %% 5.1) Reconstruct ground truth for cmin, cmax
                info = getSettings('dataset', Hsubj.dataset, 'cfg_file', fullfile(Folders.nrshfolder, Hsubj.cfg_file), 'outfolderpath', Folders.nrshOutFolderDist, ...
                'name_prefix', ['SubjTest_' plane '_GT_'], 'resize_fun', '');
                if (isfield(Hsubj, 'ap_sizes')), info = getSettings(info, 'ap_sizes', Hsubj.ap_sizes); end
                if (isfield(Hsubj, 'fps')), info = getSettings(info, 'fps', Hsubj.fps); end

                if (Hsubj.doLowResolution)
                    info = getSettings(info, 'resize_fun', 'dr');
                    info = getSettings(info, 'targetres', Hsubj.targetRes);
                end

                if (~exist('X', 'var')), X = holoread_PL(Folders); end
                Hsubj.size = size(X);

                if (~doSubjTestRatingOnly)
                    [~, cminSubjT.(plane), cmaxSubjT.(plane)] = nrsh(X, Hsubj.rec_dists, info, usagemodeFun(Hsubj.doDynamic, Hsubj.doIndividual), ...
                        [], Hsubj.h_pos, Hsubj.v_pos);
                    save(fullfile(Folders.plenofolder, 'CfgSubjTest.mat'), 'cminSubjT', 'cmaxSubjT', 'Hsubj', 'H');

                    %% 5.2) Get quantized ground truth into memory, if anchor is present
                    % FIXME: Disabled for now, until verification that its really needed
                    if (~isempty(intersect(distL, distLanchors)))
                        fname = fullfile(Folders.forkfolder, 'quantref.mat');
                        %% 5.2.a) Load quantized ground truth
                        if (exist(fname, 'file'))
                            X = load(fname, 'Xqref');
                            X = X.Xqref;
                        else
                            %% 5.2.b) Redo quantization

                            %% 5.2.b.1) Propagate to object plane evtl.
                            if (~strcmpi(plane, 'holo'))
                                zeropad = false; % Faciliate: nopad approach for anchors, see wg1m89073
                                info = getSettings('dataset', Hsubj.dataset, 'zero_pad', zeropad, 'cfg_file', fullfile(Folders.nrshfolder, Hsubj.cfg_file), 'usagemode', 'complex', 'direction', 'forward');
                                X = nrsh(X, Hsubj.obj_dist, info);
                            end

                            fname = fullfile(Folders.plenofolder, ['QuantInfo_' plane 'Plane.mat']);

                            if (exist(fname, 'file'))
                                %% 5.2.b.1a) Redo quantization exactly using prior optimal clipping points
                                disp(['Objective testing: loaded quantization information from ' fname ' to redo quantization exactly.'])
                                load(fname, 'Xpoi', 'quantmethod', 'L');
                                [~, X] = Quantize_PL(X, L, quantmethod, Xpoi);
                            else
                                %% 5.2.b.1b) Redo quantization by recalculating optimal clipping points before
                                disp(['Objective testing: could not load quantization information from ' fname '. Redo quantization optimization.'])
                                [~, X, Xpoi] = Quantize_PL(X, L, quantmethod, optmethod);
                                save(fullfile(Folders.plenofolder, ['QuantInfo_' plane 'Plane.mat']), 'Xpoi', 'quantmethod', 'optmethod', 'L');
                            end

                        end

                        %% 5.3) Reconstruct quantized ground truth + BP quantized GT
                        info = getSettings('dataset', Hsubj.dataset, 'cfg_file', fullfile(Folders.nrshfolder, Hsubj.cfg_file), 'name_prefix', ['SubjTest_' plane '_GT16bit_'], 'outfolderpath', Folders.nrshOutFolderDist, 'resize_fun', '');

                        if (isfield(Hsubj, 'ap_sizes')), info = getSettings(info, 'ap_sizes', Hsubj.ap_sizes); end
                        if (isfield(Hsubj, 'fps')), info = getSettings(info, 'fps', Hsubj.fps); end

                        if (Hsubj.doLowResolution)
                            info = getSettings(info, 'resize_fun', 'dr');
                            info = getSettings(info, 'targetres', Hsubj.targetRes);
                        end

                        if (strcmpi(plane, 'obj'))
                            zeropad = false; % Faciliate: nopad approach for anchors, see wg1m89073
                            infoComplex = getSettings('dataset', Hsubj.dataset, 'zero_pad', zeropad, 'cfg_file', fullfile(Folders.nrshfolder, Hsubj.cfg_file), 'usagemode', 'complex', 'direction', 'inverse');

                            X = nrsh(X, Hsubj.obj_dist, infoComplex); % Propagate quantized GT from obj to holo plane
                            nrsh(X, Hsubj.rec_dists, info, usagemodeFun(Hsubj.doDynamic, Hsubj.doIndividual), Hsubj.ap_sizes, Hsubj.h_pos, Hsubj.v_pos, cminSubjT.(plane), cmaxSubjT.(plane));
                        else
                            nrsh(X, Hsubj.rec_dists, info, usagemodeFun(Hsubj.doDynamic, Hsubj.doIndividual), Hsubj.ap_sizes, Hsubj.h_pos, Hsubj.v_pos, cminSubjT.(plane), cmaxSubjT.(plane));
                        end

                        clear X;
                    else
                        clear X;
                    end

                end

                %% 5.4) Reconstruct all compressed versions
                Msubj.(plane) = ST_RenderCompressedHolograms(Folders, M.(plane), Hsubj, si, cminSubjT.(plane), cmaxSubjT.(plane), plane, Hsubj.doLowResolution, distL, distLanchors, Hsubj.bitrateL, doSubjTestRatingOnly);

                %% 5.5) Save results (QPN/Bitrate map + Rating Scores)
                save(fullfile(Folders.plenofolder, ['MetricsSubj.mat']), '-v6', 'Msubj', 'cminSubjT', 'cmaxSubjT', 'Hsubj', 'distL', 'distLanchors');
                disp(['Processing ' Folders.holofile ' in ' plane ' plane - Subjective Test - done in ' num2str(toc(tLocSubj)), ' s.'])
            end

            %% 6.1) Clean up
            try
                fname = fullfile(Folders.forkfolder, 'quantref.mat');
                if (exist(fname, 'file')), delete(fname); end

                for distC = distL
                    fname = fullfile(Folders.forkfolder, distC{1}, '*bitrateMap_interim.mat');
                    if (exist(fname, 'file')), delete(fname); end
                end

            catch me
                disp('Cleanup of some temporary data (quantref.mat, *bitrateMap_interim.mat) failed.')
            end

            disp(['Processing ' Folders.holofile ' in ' plane ' plane done in ' num2str(toc(tLocPlane)), ' s.'])
        end

        %% 6.2) Clean up
        try
            fname = fullfile(Folders.plenofolder, 'input.mat');
            if (exist(fname, 'file')), delete(fname); end
        catch me
            disp('Cleanup of some temporary data (input.mat) failed.')
        end

        disp(['Processing ' Folders.holofile ' in ' plane ' plane done in ' num2str(toc(tLocHologram)), ' s.'])
        for ii = 1:10, disp(" "), end
    end

catch me
    diary off
    rethrow(me)
end

diary off
